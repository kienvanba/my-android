package com.example.myandroid.util

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

/**
 * Used as a wrapper for data that is exposed via a LiveData that represents an event.
 */
open class SingleEvent<out T>(val content: T) {

    var hasBeenHandled = false
        private set

    /**
     * Returns the content and prevents its use again.
     */
    val contentIfNotHandled: T? get() = if (hasBeenHandled) {
        null
    } else {
        hasBeenHandled = true
        content
    }
}

fun <T> T.asSingleEvent() = SingleEvent(this)

inline fun <T> LiveData<SingleEvent<T>>.observeSingle(owner: LifecycleOwner, crossinline onEventUnHandledContent: (T) -> Unit) {
    observe(owner) { it?.contentIfNotHandled?.let(onEventUnHandledContent) }
}