package com.example.myandroid.util

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

fun View.hideKeyboard() {
    val ipm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    ipm.hideSoftInputFromWindow(windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
}

fun View.showKeyboard() {
    val ipm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    ipm.showSoftInput(this, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun EditText.requestFocusWithKeyboard() {
    requestFocus()
    showKeyboard()
}

fun EditText.clearFocusWithKeyboard() {
    clearFocus()
    hideKeyboard()
}

fun View.lock(millis: Long = AppConstants.SafeClickInterval) {
    this.isEnabled = false
    Handler(Looper.getMainLooper()).postDelayed({ this.isEnabled = true }, millis)
}