package com.example.myandroid.data.network.service

import com.example.myandroid.data.network.request.LoginRequest
import com.example.myandroid.data.network.response.AppResponse
import com.example.myandroid.model.Login
import com.example.myandroid.model.User
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AuthService {
    @POST("auth/login")
    suspend fun login(@Body request: LoginRequest): AppResponse<Login>

    @GET("auth/user")
    suspend fun getUser(): AppResponse<User>
}