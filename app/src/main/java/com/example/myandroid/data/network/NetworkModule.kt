package com.example.myandroid.data.network

import android.content.Context
import com.example.myandroid.BuildConfig
import com.example.myandroid.data.local.prefs.AppPrefs
import com.google.gson.Gson
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

class NetworkModule(
    context: Context,
    private val gson: Gson,
    private val appPref: AppPrefs
) {
    companion object {
        const val TIME_OUT = 30
    }

    fun createRetrofit(): Retrofit {
        val loggingInterceptor = createLoggingInterceptor()
        val requestInterceptor = createRequestInterceptor()
        val client = createOkHttpClient(cache, loggingInterceptor, requestInterceptor)

        return Retrofit.Builder()
            .baseUrl(getBaseUrl())
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    inline fun <reified T> createApiService(): T {
        return createRetrofit().create(T::class.java)
    }

    private val cache = createOkHttpCache(context)

    private fun getBaseUrl(): String {
        return BuildConfig.API_URL
    }

    private fun createOkHttpCache(context: Context): Cache {
        val size = (10 * 1024 * 1024).toLong() // 10 Mb
        return Cache(context.cacheDir, size)
    }

    private fun createRequestInterceptor(): Interceptor {
        return Interceptor { chain ->
            val request = chain.request()
            val newRequest = request.newBuilder()
                .header("Authorization", "Bearer ${appPref.token}")
                .header("accept", "application/json")
                .method(request.method, request.body)
                .build()
            chain.proceed(newRequest)
        }
    }

    private fun createLoggingInterceptor(): Interceptor {
        val logging = HttpLoggingInterceptor { message -> Timber.d(message) }
        logging.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }

    private fun createOkHttpClient(
        cache: Cache,
        logging: Interceptor,
        request: Interceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .cache(cache)
            .connectTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
            .addInterceptor(request)
            .addInterceptor(logging)
            .build()
    }
}