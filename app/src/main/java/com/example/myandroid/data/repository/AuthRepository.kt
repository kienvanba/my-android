package com.example.myandroid.data.repository

import com.example.myandroid.data.local.prefs.AppPrefs
import com.example.myandroid.data.network.request.LoginRequest
import com.example.myandroid.data.network.response.AppResponse
import com.example.myandroid.data.network.service.AuthService
import com.example.myandroid.model.Login
import com.example.myandroid.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val service: AuthService,
    private val appPrefs: AppPrefs,
) {
    val needLogin: Boolean get() = appPrefs.token == null

    fun saveLoginData(data: Login) {
        appPrefs.token = data.accessToken
    }

    suspend fun login(request: LoginRequest): Login {
        return withContext(Dispatchers.IO) {
            service.login(request).let {
                if (it.data != null) {
                    return@withContext it.data
                }
                throw it.error
            }
        }
    }

    suspend fun getUserInfo(): User {
        return withContext(Dispatchers.IO) {
            service.getUser().let {
                if (it.data != null) {
                    return@withContext it.data
                }
                throw it.error
            }
        }
    }
}