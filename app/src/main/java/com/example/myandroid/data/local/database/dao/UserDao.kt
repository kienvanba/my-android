package com.example.myandroid.data.local.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.myandroid.model.User

@Dao
interface UserDao {
    @Query("SELECT * FROM User")
    suspend fun getAllUser(): List<User>
}