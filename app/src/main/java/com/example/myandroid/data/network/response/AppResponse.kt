package com.example.myandroid.data.network.response

import com.example.myandroid.model.AppError
import com.google.gson.annotations.SerializedName

open class AppResponse<Data> {
    @SerializedName("code") val code: Int = AppError.unknown.code
    @SerializedName("message") val message: String? = null
    @SerializedName("data") val data: Data? = null

    val error: AppError get() = AppError(message, code)
}