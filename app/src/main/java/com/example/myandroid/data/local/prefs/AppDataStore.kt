package com.example.myandroid.data.local.prefs

import android.content.Context
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

private const val DATA_STORE_NAME = "_my_android_datastore"

private val Context.dataStore by preferencesDataStore(
    name = DATA_STORE_NAME
)

class AppDataStore @Inject constructor(@ApplicationContext private val context: Context) : IPrefs {
    val keyToken = stringPreferencesKey("_token")

    override var token: String?
        get() = "null for now"
        set(value) {}
}