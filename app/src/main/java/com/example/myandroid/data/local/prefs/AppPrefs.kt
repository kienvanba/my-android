package com.example.myandroid.data.local.prefs

import android.content.Context
import androidx.core.content.edit
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class AppPrefs @Inject constructor(private val gson: Gson, @ApplicationContext context: Context): IPrefs {
    private var sharedPreferences = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)

    companion object {
        private const val TOKEN = "_token"
    }

    override var token: String?
        get() = getString(TOKEN)
        set(value) = putString(TOKEN, value)

    private fun getString(key: String, default: String? = null): String? {
        return sharedPreferences.getString(key, default)
    }

    private fun putString(key: String, value: String?, removeOnNull: Boolean = true) {
        sharedPreferences.edit {
            putString(key, value)
            if (value == null && removeOnNull) {
                remove(key)
            }
        }
    }
}