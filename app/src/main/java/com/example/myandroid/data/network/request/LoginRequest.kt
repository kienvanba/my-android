package com.example.myandroid.data.network.request

data class LoginRequest(
    val username: String,
    val password: String
)
