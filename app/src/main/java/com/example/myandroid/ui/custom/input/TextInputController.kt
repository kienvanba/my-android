package com.example.myandroid.ui.custom.input

import android.text.InputFilter
import com.google.android.material.textfield.TextInputLayout

class TextInputController(inputLayout: TextInputLayout): InputController<String>(inputLayout) {

    override var value: String? = null
        get() = inputLayout.editText?.text.toString()
        set(value) {
            field = value
            inputLayout.editText?.setText(value)
        }

    var maxTextLength: Int? = null
        set(value) {
            field = value
            if (value != null) {
                inputLayout.editText?.filters = arrayOf(InputFilter.LengthFilter(value))
            } else {
                inputLayout.editText?.filters = null
            }
        }
}
