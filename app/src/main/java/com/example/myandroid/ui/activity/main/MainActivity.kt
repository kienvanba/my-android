package com.example.myandroid.ui.activity.main

import androidx.activity.viewModels
import com.example.myandroid.R
import com.example.myandroid.base.BaseActivity
import com.example.myandroid.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {
    override val layoutId: Int = R.layout.activity_main
    override val viewModel: MainViewModel by viewModels()
}