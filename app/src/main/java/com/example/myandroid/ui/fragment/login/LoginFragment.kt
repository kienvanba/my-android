package com.example.myandroid.ui.fragment.login

import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import com.example.myandroid.R
import com.example.myandroid.base.BaseFragment
import com.example.myandroid.databinding.FragmentLoginBinding
import com.example.myandroid.ui.activity.auth.AuthViewModel
import com.example.myandroid.ui.custom.input.TextInputController
import com.example.myandroid.util.hideKeyboard
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : BaseFragment<AuthViewModel, FragmentLoginBinding>() {
    override val layoutId: Int = R.layout.fragment_login
    override val viewModel: AuthViewModel by activityViewModels()

    override fun initView() {
        super.initView()
        viewBinding.apply {
            val usernameController = TextInputController(inputUsername).apply {
                validation = { value ->
                    when {
                        value.isNullOrEmpty() -> "Field can not be empty"
                        value.length < 6 -> "Username at least contains 6 characters"
                        else -> null
                    }
                }
            }
            val passwordController = TextInputController(inputPassword).apply {
                validation = { value ->
                    when {
                        value.isNullOrEmpty() -> "Password can not empty"
                        value.length < 6 -> "Password too short"
                        else -> null
                    }
                }
            }

            btnLogin.setOnClickListener {
                if (usernameController.validated && passwordController.validated) {
                    root.hideKeyboard()
                    viewModel.login(usernameController.value!!, passwordController.value!!)
                }
            }
        }
    }

    override fun showLoading(show: Boolean) {
        viewBinding.apply {
            progressCircular.isVisible = show
            btnLogin.isVisible = !show
        }
    }
}