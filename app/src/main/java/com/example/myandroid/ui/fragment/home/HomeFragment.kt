package com.example.myandroid.ui.fragment.home

import androidx.fragment.app.viewModels
import com.example.myandroid.R
import com.example.myandroid.base.BaseFragment
import com.example.myandroid.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeViewModel, FragmentHomeBinding>() {
    override val layoutId: Int = R.layout.fragment_home
    override val viewModel: HomeViewModel by viewModels()
}