package com.example.myandroid.ui.activity.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myandroid.base.BaseViewModel
import com.example.myandroid.data.network.request.LoginRequest
import com.example.myandroid.data.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val authRepository: AuthRepository
): BaseViewModel() {
    private val _loggedIn = MutableLiveData<Boolean>()
    val loggedIn: LiveData<Boolean> = _loggedIn

    val needLogin: Boolean get() = authRepository.needLogin

    fun login(username: String, password: String) {
        execute (true) {
            val data = authRepository.login(LoginRequest(username, password))
            authRepository.saveLoginData(data)
            _loggedIn.postValue(true)
        }
    }
}