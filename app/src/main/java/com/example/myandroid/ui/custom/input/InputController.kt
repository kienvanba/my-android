package com.example.myandroid.ui.custom.input

import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import com.example.myandroid.util.clearFocusWithKeyboard
import com.example.myandroid.util.requestFocusWithKeyboard
import com.google.android.material.textfield.TextInputLayout

open class InputController<T>(protected val inputLayout: TextInputLayout) {
    open var validation: ((T?) -> String?)? = null

    init {
        inputLayout.editText?.apply {
            addTextChangedListener {
                this.error = null
            }
        }
    }

    open val validated: Boolean get() {
        val error = validation?.invoke(value)
        if (error != null) {
            requestFocus()
        }
        inputLayout.error = error
        return error == null
    }

    open var error: String?
        get() = inputLayout.error.toString()
        set(value) { inputLayout.error = value }

    open var value: T? = null

    open fun requestFocus() {
        inputLayout.editText?.requestFocusWithKeyboard()
    }

    open fun setOnDoneListener(shouldValidate: Boolean = true, listener: (T?) -> Unit) {
        inputLayout.editText?.setOnEditorActionListener { _ , actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (shouldValidate && validated) {
                    inputLayout.editText?.clearFocusWithKeyboard()
                    listener.invoke(value)
                    return@setOnEditorActionListener true
                }
                return@setOnEditorActionListener false
            }
            return@setOnEditorActionListener false
        }
    }
}
