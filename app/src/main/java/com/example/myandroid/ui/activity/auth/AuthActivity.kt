package com.example.myandroid.ui.activity.auth

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.myandroid.R
import com.example.myandroid.base.BaseActivity
import com.example.myandroid.databinding.ActivityAuthBinding
import com.example.myandroid.ui.activity.main.MainActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthActivity : BaseActivity<AuthViewModel, ActivityAuthBinding>() {
    override val layoutId: Int = R.layout.activity_auth
    override val viewModel: AuthViewModel by viewModels()

    private var _shouldShowSplashScreen = true
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        initSplashScreen()
        super.onCreate(savedInstanceState)
    }

    private fun initSplashScreen() {
        installSplashScreen().apply {
            setKeepVisibleCondition { _shouldShowSplashScreen }
        }
    }

    override fun initView() {
        super.initView()
        checkLogin()

        val navHostFragment = supportFragmentManager.findFragmentById(
            R.id.fragment_container
        ) as NavHostFragment
        navController = navHostFragment.navController
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun initObserver() {
        super.initObserver()
        viewModel.loggedIn.observe(this, {
            if (it) {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        })
    }

    private fun checkLogin() {
        if (!viewModel.needLogin) {
            startActivity(Intent(this, MainActivity::class.java))
        }
        _shouldShowSplashScreen = false
    }

    override fun onNavigateUp(): Boolean {
        return navController.navigateUp() || super.onNavigateUp()
    }
}