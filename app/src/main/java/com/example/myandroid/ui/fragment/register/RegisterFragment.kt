package com.example.myandroid.ui.fragment.register

import androidx.fragment.app.activityViewModels
import com.example.myandroid.R
import com.example.myandroid.base.BaseFragment
import com.example.myandroid.databinding.FragmentRegisterBinding
import com.example.myandroid.ui.activity.auth.AuthViewModel
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class RegisterFragment : BaseFragment<AuthViewModel, FragmentRegisterBinding>() {
    override val layoutId: Int = R.layout.fragment_register
    override val viewModel: AuthViewModel by activityViewModels()

    override fun initView() {
        super.initView()
        Timber.d("init view >> register fragment")
    }
}