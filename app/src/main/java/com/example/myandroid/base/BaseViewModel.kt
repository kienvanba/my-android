package com.example.myandroid.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myandroid.model.AppError
import kotlinx.coroutines.launch
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException

open class BaseViewModel : ViewModel() {
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading
    private val _error = MutableLiveData<AppError>()
    val error: LiveData<AppError> = _error

    protected fun <T> execute(showLoading: Boolean = true, fn: suspend () -> T) {
        viewModelScope.launch {
            try {
                if (showLoading) showLoading()
                fn()
                if (showLoading) hideLoading()
            } catch (throwable: Throwable) {
                handleError(throwable)
            }
        }
    }

    protected open fun hideLoading() {
        val loading = _isLoading.value ?: false
        if (loading) {
            _isLoading.postValue(false)
        }
    }

    protected open fun showLoading() {
        val loading = _isLoading.value ?: false
        if (!loading) {
            _isLoading.postValue(true)
        }
    }

    protected open fun handleError(throwable: Throwable) {
        Timber.e(throwable)
        hideLoading()
        val error = if (throwable is AppError) {
            throwable
        } else {
            when (throwable) {
                is IOException -> AppError.noInternet
                is HttpException -> AppError.fromHttp(throwable)
                else -> AppError.unknown
            }
        }
        handleAppError(error)
    }

    protected open fun handleAppError(error: AppError) {
        _error.postValue(error)
    }
}