package com.example.myandroid.base

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.myandroid.model.AppError

abstract class BaseActivity<ViewModel: BaseViewModel, ViewBinding: ViewDataBinding>: AppCompatActivity() {
    protected abstract val viewModel: ViewModel
    @get:LayoutRes
    protected abstract val layoutId: Int

    protected lateinit var viewBinding: ViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = DataBindingUtil.setContentView(this, layoutId) as ViewBinding
        viewBinding.apply {
            setBindingVariable(this)
            root.isClickable = true
            lifecycleOwner = this@BaseActivity
            executePendingBindings()
        }

        initView()
        initObserver()
    }

    open fun initView() {}
    open fun initObserver() {
        viewModel.apply {
            isLoading.observe(this@BaseActivity, {
                showLoading(it)
            })
            error.observe(this@BaseActivity, {
                errorAlert(it)
            })
        }
    }

    open fun setBindingVariable(viewBinding: ViewBinding) {}

    protected fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    protected open fun errorAlert(error: AppError) {
        toast(error.requiredMessage)
    }

    protected open fun showLoading(show: Boolean) {}
}