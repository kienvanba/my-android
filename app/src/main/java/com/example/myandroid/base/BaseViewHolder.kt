package com.example.myandroid.base

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

open class BaseViewHolder<ViewBinding: ViewDataBinding>(open val binding: ViewBinding) : RecyclerView.ViewHolder(binding.root)