package com.example.myandroid.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.example.myandroid.model.AppError

abstract class BaseFragment<ViewModel: BaseViewModel, ViewBinding: ViewDataBinding> : Fragment() {
    @get:LayoutRes
    protected abstract val layoutId: Int
    abstract val viewModel: ViewModel

    private var _viewBinding: ViewBinding? = null
    protected val viewBinding: ViewBinding
        get() = _viewBinding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (_viewBinding != null) {
            viewBinding
        } else {
            _viewBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
            viewBinding.apply {
                setBindingVariable(this)
                root.isClickable = true
                executePendingBindings()
            }
        }
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initObserver()
    }

    open fun initView() {}
    open fun initObserver() {
        viewModel.apply {
            error.observe(viewLifecycleOwner, {
                errorAlert(it)
            })
            isLoading.observe(viewLifecycleOwner, {
                showLoading(it)
            })
        }
    }

    open fun setBindingVariable(viewBinding: ViewBinding) {}

    protected fun toast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    protected open fun errorAlert(error: AppError) {
        toast(error.requiredMessage)
    }

    protected open fun showLoading(show: Boolean) {}

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }
}