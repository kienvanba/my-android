package com.example.myandroid.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import java.util.concurrent.Executors

abstract class BaseAdapter<Item, ViewBinding : ViewDataBinding>(callback: DiffUtil.ItemCallback<Item>) :
    ListAdapter<Item, BaseViewHolder<ViewBinding>>(
        AsyncDifferConfig.Builder(callback).setBackgroundThreadExecutor(
            Executors.newSingleThreadExecutor()
        ).build()
    ) {

    @get:LayoutRes
    abstract val layoutId: Int
    abstract val itemBindingVariable: Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<ViewBinding> {
        return BaseViewHolder(
            DataBindingUtil.inflate<ViewBinding>(
                LayoutInflater.from(parent.context),
                layoutId,
                parent,
                false
            ).apply {
                bindFirstTime(this)
            })
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewBinding>, position: Int) {
        onBindViewHolder(holder, position, mutableListOf())
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<ViewBinding>,
        position: Int,
        payloads: MutableList<Any>
    ) {
        try {
            val item: Item = getItem(position)
            holder.binding.setVariable(itemBindingVariable, item)
            setBindingVariable(holder.binding)
            bindView(holder.binding, item, position)
            bindWithPayloads(holder.binding, item, position, payloads)
        } catch (e: IndexOutOfBoundsException) {
            bind(holder.binding, position)
        }
        holder.binding.executePendingBindings()
    }

    protected open fun setBindingVariable(viewBinding: ViewBinding) {}

    protected open fun bindFirstTime(binding: ViewBinding) {}

    protected open fun bindWithPayloads(binding: ViewBinding, item: Item, position: Int, payloads: MutableList<Any>) {}

    protected open fun bindView(binding: ViewBinding, item: Item, position: Int) {}

    protected open fun bind(binding: ViewBinding, position: Int) {}

}