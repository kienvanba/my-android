package com.example.myandroid.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class Login(
    @SerializedName("access_token") val accessToken: String,
    @SerializedName("expired_at") val date: Date,
    @SerializedName("refresh_token") val refreshToken: String,
)
