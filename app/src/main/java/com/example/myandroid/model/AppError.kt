package com.example.myandroid.model

import retrofit2.HttpException

data class AppError(override val message: String?, val code: Int = 0): Throwable() {
    companion object {
        private const val codeNoInternet = -1
        private const val codeUnknown = -999
        private const val msgUnknown = "Unknown Error"
        val noInternet = AppError("No internet access", codeNoInternet)
        val unknown = AppError(msgUnknown, codeUnknown)
        fun fromHttp(throwable: HttpException) = AppError(throwable.message(), throwable.code())
    }

    val requiredMessage: String get() = message ?: "$msgUnknown [$code]"
}