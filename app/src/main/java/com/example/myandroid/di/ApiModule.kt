package com.example.myandroid.di

import android.content.Context
import com.example.myandroid.data.local.prefs.AppPrefs
import com.example.myandroid.data.network.NetworkModule
import com.example.myandroid.data.network.service.AuthService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module object ApiModule {
    @Singleton
    @Provides
    fun provideNetworkModule(@ApplicationContext context: Context, gson: Gson, appPrefs: AppPrefs): NetworkModule {
        return NetworkModule(context, gson, appPrefs)
    }

    @Provides
    @Singleton
    fun provideRetrofit(module: NetworkModule): Retrofit {
        return module.createRetrofit()
    }

    @Provides
    fun provideAuthService(module: NetworkModule): AuthService {
        return module.createApiService()
    }
}