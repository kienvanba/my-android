package com.example.myandroid.di

import com.example.myandroid.data.local.prefs.AppDataStore
import com.example.myandroid.data.local.prefs.AppPrefs
import com.example.myandroid.data.local.prefs.IPrefs
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module abstract class PrefsModule {
    @Binds
    @Singleton
    abstract fun bindAppPrefs(appPrefs: AppPrefs): IPrefs

    @Binds
    @Singleton
    abstract fun bindAppDataStore(dataStore: AppDataStore): IPrefs
}