package com.example.myandroid.di

import android.content.Context
import android.content.res.Resources
import com.example.myandroid.App
import com.example.myandroid.util.ApiConstants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object AppModule {

    @Provides
    fun provideResources(@ApplicationContext context: Context): Resources {
        return context.resources
    }

    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().setDateFormat(ApiConstants.DATE_FORMAT).create()
    }
}